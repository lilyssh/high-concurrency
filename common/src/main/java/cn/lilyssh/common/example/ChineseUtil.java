package cn.lilyssh.common.example;

import java.io.UnsupportedEncodingException;
import java.util.Random;

public class ChineseUtil {
    //随机生成汉字
    //seed指定Random(long seed)中的种子数
    private static String getOneChinese(long seed){
        String str=null;
        int highpos,lowpos;
        Random random=new Random(seed);
        highpos=(176+Math.abs(random.nextInt(39)));
        lowpos=(161+Math.abs(random.nextInt(93)));
        byte[] bb=new byte[2];
        bb[0]=new Integer(highpos).byteValue();
        bb[1]=new Integer(lowpos).byteValue();
        //String(byte[] bytes, Charset charset)
        //通过使用指定的 charset 解码指定的 byte 数组，构造一个新的 String。
        try {
            str=new String(bb,"GBK");
        } catch (UnsupportedEncodingException e) {
            return "张三";
        }
        return str;
    }

    public static String getChinese(int num){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < num; i++) {
            Random random=new Random();
            sb.append(ChineseUtil.getOneChinese(random.nextInt(99999999)));
        }
        return sb.toString();
    }
}
