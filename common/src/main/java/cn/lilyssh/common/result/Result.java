package cn.lilyssh.common.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 统一返回类型规范
 */

@ApiModel(description = "返回响应数据")
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态码")
    private String code = ReturnCode.OK.code();

    @ApiModelProperty(value = "错误信息")
    private String message = ReturnCode.OK.message();

    @ApiModelProperty(value = "返回的数据")
    private T result;

    public static<T> Result<T> result(T result){
        Result<T> tResult = new Result<>();
        tResult.setResult(result);
        return tResult;
    }

    public static<T> Result<T> ok(){
        Result<T> tResult = new Result<>();
        return tResult;
    }

    public static<T> Result<T> code(ReturnCodeInterFace returnCodeInterFace){
        Result<T> tResult = new Result<>();
        if(returnCodeInterFace != null){
            tResult.code = returnCodeInterFace.code();
            tResult.message = returnCodeInterFace.message();
        }
        return tResult;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Result<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getResult() {
        return result;
    }

    public Result<T> setResult(T result) {
        this.result = result;
        return this;
    }

    public Result<T> setCode(ReturnCodeInterFace returnCodeInterFace){
        if(returnCodeInterFace != null){
            this.code = returnCodeInterFace.code();
            this.message = returnCodeInterFace.message();
        }
        return this;
    }

    /**
     * 方法对比的是code的值
     * @param returnCodeInterFace
     * @return
     */
    public boolean equals(ReturnCodeInterFace returnCodeInterFace){
        if(returnCodeInterFace != null){
            return this.code.equals(returnCodeInterFace.code());
        }
        return Boolean.FALSE;
    }

}
