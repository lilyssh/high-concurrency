package cn.lilyssh.common.result;

/**
 * 返回编码
 */

public enum ReturnCode implements ReturnCodeInterFace {
    /**
     * 数据处理成功
     */
    OK("OK", "Success!"),
    /**
     * 数据处理失败，如：保存、发送
     */
    FAIL("SYS001", "Processing failed!"),
    /**
     * 数据信息不存在
     */
    NOT_EXITS("SYS002", "Data does not exist!"),

    /**
     * 数据重复
     */
    EXITS("SYS003", "The data has already existed!"),
    /**
     * 未知错误
     */
    UNKNOW("SYS004", "unknown error!"),
    /**
     * 异常
     */
    EXCEPTION("SYS005", "Server handling exceptions!"),
    /**
     * 用户输入或接口入参缺少
     */
    PARAM_ERROR("SYS006", "Parameter error!"),
    /**
     * 库存不足
     */
    LOW_STOCKS("SYS008", "low stocks!"),
    /**
     * 系统应用间通讯超时
     */
    TIMEOUT("SYS007", "timeout!");



    /**
     * 错误编号
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    ReturnCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }


    @Override
    public String message() {
        return message;
    }

}
