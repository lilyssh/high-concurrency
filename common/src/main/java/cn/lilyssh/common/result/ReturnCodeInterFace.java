package cn.lilyssh.common.result;

/**
 * 返回编码接口
 */
public interface ReturnCodeInterFace {
    /**
     * 获取返回编码
     * @return code
     */
    String code();

    /**
     * 获取返回描述信息
     * @return message
     */
    String message();

}
