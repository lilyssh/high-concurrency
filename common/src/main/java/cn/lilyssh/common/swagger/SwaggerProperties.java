package cn.lilyssh.common.swagger;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "swagger")
public class SwaggerProperties {
    private boolean enable = true;
    private String title;
    private String description;
    private String author;
    private String email;
    private String version;
    private String basePackage;
    private String url;
}
