package cn.lilyssh.common.validate;

public class ValidateGroup {

    public interface Insert {}
    public interface Delete {}
    public interface Update {}
    public interface Select {}
}
