package cn.lilyssh.goods.api.model.request;


import cn.lilyssh.common.validate.ValidateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class GoodsQueryReq implements Serializable {
    private Integer id;
    @NotNull(groups = ValidateGroup.Insert.class)
    private String goodsName;
    private int stock;
    private String logo;
    private String smLogo;
    private BigDecimal price;
    private String goodsDesc;
    private int isOnSale;
    private int isDelete;
    private Date createTime ;
    private Date updateTime;
}
