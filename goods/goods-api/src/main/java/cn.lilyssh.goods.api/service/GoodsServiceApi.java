package cn.lilyssh.goods.api.service;

import cn.lilyssh.goods.api.model.request.GoodsInsertReq;
import cn.lilyssh.goods.api.model.request.GoodsQueryReq;
import cn.lilyssh.goods.api.model.request.GoodsUpdateReq;
import cn.lilyssh.goods.api.model.response.GoodsQueryResp;

import java.util.List;

public interface GoodsServiceApi {

    /**
     * 获取所有商品
     */
    List<GoodsQueryResp> list(GoodsQueryReq orderQueryReq);

    /**
     * 新增商品
     */
    boolean save(GoodsInsertReq orderInsertReq);

    /**
     * 批量新增商品
     */
    boolean saveBatch(List<GoodsInsertReq> orderInsertReqList);

    /**
     * 根据goodsId判断商品是否存在
     * @return true：存在  false：不存在
     */
    boolean exist(Integer goodsId);

    /**
     * 更新
     * @return
     */
    boolean updateById(GoodsUpdateReq goodsUpdateReq);
    /**
     * 造数据
     */
    void initData(Integer goodsNum);
}
