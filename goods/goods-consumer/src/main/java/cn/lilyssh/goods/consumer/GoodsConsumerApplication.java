package cn.lilyssh.goods.consumer;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubboConfiguration
public class GoodsConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoodsConsumerApplication.class, args);
	}
}
