package cn.lilyssh.goods.consumer.controller;

import cn.lilyssh.common.result.Result;
import cn.lilyssh.common.validate.ValidateGroup;
import cn.lilyssh.goods.api.model.request.GoodsInsertReq;
import cn.lilyssh.goods.api.model.request.GoodsQueryReq;
import cn.lilyssh.goods.consumer.service.GoodsService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/goods")
public class GoodsController {

    private GoodsService goodsService;

    /**
     * 获取所有订单
     */
    @GetMapping
    public Result goodsList(GoodsQueryReq goodsQueryReq){
        return goodsService.goodsList(goodsQueryReq);
    }

    /**
     * 下单
     */
    @PostMapping
    public Result save(@RequestBody @Validated(value = ValidateGroup.Insert.class) GoodsInsertReq goodsInsertReq){
        return goodsService.save(goodsInsertReq);
    }

    /**
     * 造数据
     */
    @GetMapping("/{goodsNum}")
    public Result initData(@PathVariable Integer goodsNum){
        return goodsService.initData(goodsNum);
    }

}
