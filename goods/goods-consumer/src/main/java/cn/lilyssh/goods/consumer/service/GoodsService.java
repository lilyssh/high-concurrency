package cn.lilyssh.goods.consumer.service;

import cn.lilyssh.common.result.Result;
import cn.lilyssh.common.result.ReturnCode;
import cn.lilyssh.goods.api.model.request.GoodsInsertReq;
import cn.lilyssh.goods.api.model.request.GoodsQueryReq;
import cn.lilyssh.goods.api.model.response.GoodsQueryResp;
import cn.lilyssh.goods.api.service.GoodsServiceApi;
import cn.lilyssh.user.api.service.UserServiceApi;
import com.alibaba.dubbo.config.annotation.Reference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class GoodsService {

    @Reference
    private GoodsServiceApi goodsServiceApi;

    @Reference
    private UserServiceApi userServiceApi;

    public Result<List<GoodsQueryResp>> goodsList(GoodsQueryReq goodsQueryReq){
        List<GoodsQueryResp> list = goodsServiceApi.list(goodsQueryReq);
        return Result.result(list);
    }

    public Result save(GoodsInsertReq goodsInsertReq){
        return Result.result("");
    }

    public Result initData(Integer goodsNum){
        Thread thread = new Thread(() -> {
            goodsServiceApi.initData(goodsNum);
        });
        thread.start();
        return Result.ok();
    }

}
