package cn.lilyssh.goods.provider;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@Slf4j
@SpringBootApplication
@EnableDubboConfiguration
public class GoodsProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoodsProviderApplication.class, args);
		log.info("goods-provider started ~~~~~~~~~~~~~~~~~~");
	}
}
