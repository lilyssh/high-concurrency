package cn.lilyssh.goods.provider.dao.entity;

import cn.lilyssh.common.validate.ValidateGroup;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("`goods`")
public class GoodsEntity {
    private Integer id;
    @NotNull(groups = ValidateGroup.Insert.class)
    private String goodsName;
    private int stock;
    private String logo;
    private String smLogo;
    private BigDecimal price;
    private String goodsDesc;
    private int isOnSale;
    private int isDelete;
    private Date createTime ;
    private Date updateTime;
}
