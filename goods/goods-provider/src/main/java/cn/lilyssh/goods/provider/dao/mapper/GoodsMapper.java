package cn.lilyssh.goods.provider.dao.mapper;

import cn.lilyssh.goods.provider.dao.entity.GoodsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GoodsMapper extends BaseMapper<GoodsEntity> {

}