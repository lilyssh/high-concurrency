package cn.lilyssh.goods.provider.dao.repository;

import cn.lilyssh.goods.provider.dao.entity.GoodsEntity;
import cn.lilyssh.goods.provider.dao.mapper.GoodsMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class GoodsRepository extends ServiceImpl<GoodsMapper, GoodsEntity> {

}
