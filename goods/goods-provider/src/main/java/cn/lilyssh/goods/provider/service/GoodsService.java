package cn.lilyssh.goods.provider.service;


import cn.lilyssh.common.example.ChineseUtil;
import cn.lilyssh.goods.api.model.request.GoodsInsertReq;
import cn.lilyssh.goods.api.model.request.GoodsQueryReq;
import cn.lilyssh.goods.api.model.request.GoodsUpdateReq;
import cn.lilyssh.goods.api.model.response.GoodsQueryResp;
import cn.lilyssh.goods.api.service.GoodsServiceApi;
import cn.lilyssh.goods.provider.dao.entity.GoodsEntity;
import cn.lilyssh.goods.provider.dao.repository.GoodsRepository;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Slf4j
@Service
@Component
@AllArgsConstructor
public class GoodsService implements GoodsServiceApi {

    private GoodsRepository goodsRepository;

    @Override
    public List<GoodsQueryResp> list(GoodsQueryReq goodsQueryReq){
        QueryWrapper<GoodsEntity> qw = new QueryWrapper<>();
        if(null!=goodsQueryReq){
            if(!StringUtils.isEmpty(goodsQueryReq.getId())){
                qw.eq("id",goodsQueryReq.getId());
            }
            if(!StringUtils.isEmpty(goodsQueryReq.getGoodsName())){
                qw.like("goods_name",goodsQueryReq.getGoodsName());
            }
        }
        List<GoodsEntity> goodsEntityList = goodsRepository.list(qw);
        List<GoodsQueryResp> resultList=new ArrayList<>();
        goodsEntityList.forEach(goodsEntity->{
            GoodsQueryResp  goodsQueryResp=new GoodsQueryResp();
            BeanUtils.copyProperties(goodsEntity,goodsQueryResp);
            resultList.add(goodsQueryResp);
        });
        return resultList;
    }

    @Override
    public boolean save(GoodsInsertReq goodsInsertReq){
        GoodsEntity goodsEntity=new GoodsEntity();
        BeanUtils.copyProperties(goodsInsertReq,goodsEntity);
        return goodsRepository.save(goodsEntity);
    }

    @Override
    public boolean saveBatch(List<GoodsInsertReq> goodsInsertReqList) {
        List<GoodsEntity> goodsEntityList = new ArrayList<>();
        goodsInsertReqList.forEach(goodsInsertReq -> {
            GoodsEntity goodsEntity = new GoodsEntity();
            BeanUtils.copyProperties(goodsInsertReq,goodsEntity);
            goodsEntityList.add(goodsEntity);
        });
        return goodsRepository.saveBatch(goodsEntityList);
    }

    @Override
    public boolean exist(Integer goodsId) {
        QueryWrapper<GoodsEntity> qw = new QueryWrapper<>();
        qw.eq("id",goodsId);
        GoodsEntity goodsEntity = goodsRepository.getOne(qw);
        if(null!=goodsEntity && goodsEntity.getId()>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean updateById(GoodsUpdateReq goodsUpdateReq) {
        GoodsEntity goodsEntity=new GoodsEntity();
        BeanUtils.copyProperties(goodsUpdateReq,goodsEntity);
        return goodsRepository.updateById(goodsEntity);
    }

    @Override
    public void initData(Integer goodsNum) {
        Random ra = new Random();
        Date now = new Date();
        for(int i=0;i<goodsNum;i++){
            //1、新增商品
            GoodsInsertReq goodsInsertReq=new GoodsInsertReq();
            goodsInsertReq.setCreateTime(now);
            goodsInsertReq.setGoodsName(ChineseUtil.getChinese(3));
            goodsInsertReq.setGoodsDesc(ChineseUtil.getChinese(5));
            goodsInsertReq.setIsDelete(ra.nextInt(2));
            goodsInsertReq.setIsOnSale(ra.nextInt(2));
            goodsInsertReq.setLogo("http://ofngo1cge.bkt.clouddn.com/banner_balloon.jpg");
            goodsInsertReq.setPrice(new BigDecimal(ra.nextDouble()).multiply(new BigDecimal(1000)).setScale(2, RoundingMode.HALF_UP));
            goodsInsertReq.setStock(ra.nextInt(1000));
            goodsInsertReq.setSmLogo("http://ofngo1cge.bkt.clouddn.com/banner_children.jpg");
            goodsInsertReq.setUpdateTime(now);

            boolean result = this.save(goodsInsertReq);
            if(result){
                log.info("新增商品成功！");
            }
        }
    }

}
