package cn.lilyssh.order.api.model.request;


import cn.lilyssh.common.validate.ValidateGroup;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderInsertReq implements Serializable {
    private Integer id;
    @NotNull(groups = ValidateGroup.Insert.class)
    private Integer userId;
    private String userUuid;
    private BigDecimal payment;
    private Integer payType;
    private BigDecimal postFee;
    private Integer status;
    private Date createTime;
    private Date updateTime;
    private Date payTime;
    private Date cosignTime;
    private Date endTime;
    private Date closeTime;
    private String shippingName;
    private String shippingCode;
    private Integer goodsId;
}
