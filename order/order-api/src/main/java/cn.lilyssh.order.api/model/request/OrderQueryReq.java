package cn.lilyssh.order.api.model.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.properties.BaseIntegerProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(description = "订单查询请求数据")
public class OrderQueryReq implements Serializable {

    @ApiModelProperty(value = "订单ID",example = "123")
    private Integer id;
    @ApiModelProperty(value = "用户ID",example = "123")
    private Integer userId;
    private String userUuid;
    private BigDecimal payment;
    private Integer payType;
    private BigDecimal postFee;
    private Integer status;
    private Date createTime;
    private Date updateTime;
    private Date payTime;
    private Date cosignTime;
    private Date endTime;
    private Date closeTime;
    private String shippingName;
    private String shippingCode;

}
