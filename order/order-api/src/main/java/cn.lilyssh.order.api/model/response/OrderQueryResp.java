package cn.lilyssh.order.api.model.response;


import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderQueryResp implements Serializable {
    private Integer id;
    private Integer userId;
    private String userUuid;
    private BigDecimal payment;
    private Integer payType;
    private BigDecimal postFee;
    private Integer status;
    private Date createTime;
    private Date updateTime;
    private Date payTime;
    private Date cosignTime;
    private Date endTime;
    private Date closeTime;
    private String shippingName;
    private String shippingCode;
}
