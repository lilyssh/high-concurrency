package cn.lilyssh.order.api.service;

import cn.lilyssh.order.api.model.request.OrderInsertReq;
import cn.lilyssh.order.api.model.request.OrderQueryReq;
import cn.lilyssh.order.api.model.response.OrderQueryResp;

import java.util.List;

public interface OrderServiceApi {

    /**
     * 获取订单
     */
    List<OrderQueryResp> list(OrderQueryReq orderQueryReq);

    /**
     * 获取最新两笔订单
     */
    List<OrderQueryResp> latestOrder();

    /**
     * 下单
     */
    boolean save(OrderInsertReq orderInsertReq);

    /**
     * 批量下单
     */
    boolean saveBatch(List<OrderInsertReq> orderInsertReqList);

    void saveByKafka(OrderInsertReq orderInsertReq);

    /**
     * 获取所有订单
     */
//    List<OrderQueryResp> listByEs(OrderQueryReq orderQueryReq);

//    boolean saveByEs(OrderInsertReq orderInsertReq);
}
