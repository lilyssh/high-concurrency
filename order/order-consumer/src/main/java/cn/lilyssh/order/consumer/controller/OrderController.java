package cn.lilyssh.order.consumer.controller;

import cn.lilyssh.common.result.Result;
import cn.lilyssh.common.validate.ValidateGroup;
import cn.lilyssh.order.api.model.request.OrderInsertReq;
import cn.lilyssh.order.api.model.request.OrderQueryReq;
import cn.lilyssh.order.consumer.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(description = "订单接口")
@RestController
@AllArgsConstructor
@RequestMapping("/order")
public class OrderController {

    private OrderService orderService;

    @ApiOperation("根据条件获取订单列表")
    @GetMapping
    public Result orderList(OrderQueryReq orderQueryReq){
        return orderService.orderList(orderQueryReq);
    }

    @ApiOperation("获取最新两条订单")
    @GetMapping(value = "latestOrder")
    public Result latestOrder(OrderQueryReq orderQueryReq){
        return orderService.latestOrder();
    }

    @ApiOperation("下单")
    @PostMapping
    public Result save(@RequestBody @Validated(value = ValidateGroup.Insert.class) OrderInsertReq orderInsertReq){
        return orderService.save(orderInsertReq);
    }

}