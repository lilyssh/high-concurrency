package cn.lilyssh.order.consumer.service;

import cn.lilyssh.common.result.Result;
import cn.lilyssh.common.result.ReturnCode;
import cn.lilyssh.goods.api.model.request.GoodsQueryReq;
import cn.lilyssh.goods.api.model.request.GoodsUpdateReq;
import cn.lilyssh.goods.api.model.response.GoodsQueryResp;
import cn.lilyssh.goods.api.service.GoodsServiceApi;
import cn.lilyssh.order.api.model.request.OrderInsertReq;
import cn.lilyssh.order.api.model.request.OrderQueryReq;
import cn.lilyssh.order.api.model.response.OrderQueryResp;
import cn.lilyssh.order.api.service.OrderServiceApi;
import cn.lilyssh.user.api.service.UserServiceApi;
import com.alibaba.dubbo.config.annotation.Reference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Slf4j
@Component
public class OrderService {

    @Reference
    private OrderServiceApi orderServiceApi;

    @Reference
    private UserServiceApi userServiceApi;

    @Reference
    private GoodsServiceApi goodsServiceApi;

    public Result<List<OrderQueryResp>> orderList(OrderQueryReq orderQueryReq){
//        List<OrderQueryResp> list = orderServiceApi.list(orderQueryReq);
        List<OrderQueryResp> list = orderServiceApi.list(orderQueryReq);
        return Result.result(list);
    }

    public Result<List<OrderQueryResp>> latestOrder(){
        List<OrderQueryResp> list = orderServiceApi.latestOrder();
        return Result.result(list);
    }

    public Result save(OrderInsertReq orderInsertReq){
        //校验用户是否存在
        boolean flag=userServiceApi.exist(orderInsertReq.getUserId());
        if(!flag){
            return Result.code(ReturnCode.NOT_EXITS).setMessage("用户不存在！");
        }
        //校验商品是否存在
        flag=goodsServiceApi.exist(orderInsertReq.getUserId());
        if(!flag){
            return Result.code(ReturnCode.NOT_EXITS).setMessage("商品不存在！");
        }
        //校验库存是否足够
        GoodsQueryReq goodsQueryReq=new GoodsQueryReq();
        goodsQueryReq.setId(orderInsertReq.getGoodsId());
        List<GoodsQueryResp> goodsList = goodsServiceApi.list(goodsQueryReq);
        if(!CollectionUtils.isEmpty(goodsList)){
            GoodsQueryResp goodsQueryResp = goodsList.get(0);
            if (goodsQueryResp.getStock()>0){
                //下单
                orderServiceApi.saveByKafka(orderInsertReq);
                log.info("已调用saveByKafka");
                if(true){  //如果下单成功，修改库存。
                    GoodsUpdateReq goodsUpdateReq = new GoodsUpdateReq();
                    goodsUpdateReq.setId(goodsQueryResp.getId());
                    goodsUpdateReq.setStock(goodsQueryResp.getStock()-1);
                    boolean updateResult = goodsServiceApi.updateById(goodsUpdateReq);
                    if(updateResult){
                        log.info("修改库存成功！");
                        return Result.result("下单成功！").setCode(ReturnCode.OK);
                    }else{
                        log.error("修改库存失败！");
                        return Result.result("修改库存失败！").setCode(ReturnCode.FAIL);
                    }
                }else{
                    log.error("下单失败！");
                    return Result.result("下单失败！").setCode(ReturnCode.FAIL);
                }

            }else{
                return Result.result("库存不足！").setCode(ReturnCode.LOW_STOCKS);
            }
        }else{
            return Result.result("商品不存在！").setCode(ReturnCode.NOT_EXITS);
        }

    }

}
