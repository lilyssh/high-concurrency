//package cn.lilyssh.order.provider.dao.entity;
//
//import lombok.Data;
//import org.springframework.data.elasticsearch.annotations.Document;
//
//import java.io.Serializable;
//import java.util.Date;
//
//@Data
//@Document(indexName = "demo",type = "order", shards = 1,replicas = 0, refreshInterval = "-1")
//public class OrderESEntity implements Serializable {
//
//    private static final long serialVersionUID = 551589397625941750L;
//
//    private Integer id;
//    private Integer userId;
//    private String userUuid;
//    private Date createTime;
//    private String shippingName;
//
//}
