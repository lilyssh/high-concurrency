package cn.lilyssh.order.provider.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("`order`")
public class OrderEntity implements Serializable {

    private static final long serialVersionUID = 551589397625941750L;

//    @Id
    private Integer id;
//    @Field
    private Integer userId;
//    @Field
    private String userUuid;
//    @Field
    private BigDecimal payment;
//    @Field
    private Integer payType;
//    @Field
    private BigDecimal postFee;
//    @Field
    private Integer status;
//    @Field
    private Date createTime;
//    @Field
    private Date updateTime;
//    @Field
    private Date payTime;
//    @Field
    private Date cosignTime;
//    @Field
    private Date endTime;
//    @Field
    private Date closeTime;
//    @Field
    private String shippingName;
//    @Field
    private String shippingCode;

}
