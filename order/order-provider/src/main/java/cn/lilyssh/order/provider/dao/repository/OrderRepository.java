package cn.lilyssh.order.provider.dao.repository;

import cn.lilyssh.order.provider.dao.entity.OrderEntity;
import cn.lilyssh.order.provider.dao.mapper.OrderMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class OrderRepository extends ServiceImpl<OrderMapper, OrderEntity> {

}
