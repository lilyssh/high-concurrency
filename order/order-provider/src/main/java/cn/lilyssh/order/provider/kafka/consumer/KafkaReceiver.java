package cn.lilyssh.order.provider.kafka.consumer;

import cn.lilyssh.order.provider.dao.entity.OrderEntity;
import cn.lilyssh.order.provider.dao.repository.OrderRepository;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
@AllArgsConstructor
public class KafkaReceiver {

    private OrderRepository orderRepository;
    /**
     * 下单
     */
    @KafkaListener(topics = {"placeOrder"})
    public void listen(String orderEntityStr) {
//        log.info("------------------ orderEntityStr =" + orderEntityStr);
        Gson gs = new Gson();
        OrderEntity orderEntity = gs.fromJson(orderEntityStr, OrderEntity.class);//把JSON字符串转为对象
        orderRepository.save(orderEntity);
    }
}
