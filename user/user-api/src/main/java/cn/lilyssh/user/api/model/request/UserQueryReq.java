package cn.lilyssh.user.api.model.request;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UserQueryReq implements Serializable {
    private int id;
    private String uuid;
    private String userName;
    private String password;
    private String realName;
    private int sex;
    private int age;
    private int phone;
    private String email;
    private int status;
    private String lastLoginIp;
    private Date lastLoginTime;
    private int idType;
    private String idNumber;

}
