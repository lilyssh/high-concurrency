package cn.lilyssh.user.api.service;

import cn.lilyssh.user.api.model.request.UserInsertReq;
import cn.lilyssh.user.api.model.request.UserQueryReq;
import cn.lilyssh.user.api.model.response.UserQueryResp;

import java.util.List;

public interface UserServiceApi {

    /**
     * 获取所有用户
     * @return
     */
    List<UserQueryResp> list(UserQueryReq userQueryReq);

    /**
     * 根据userId判断用户是否存在
     * @param userId
     * @return true：存在  false：不存在
     */
    boolean exist(Integer userId);

    /**
     * 新增用户
     * @param orderInsertReq
     * @return
     */
    UserQueryResp save(UserInsertReq orderInsertReq);

    /**
     * 造数据
     */
    void initData(Integer userNum,Integer orderNum);
}
