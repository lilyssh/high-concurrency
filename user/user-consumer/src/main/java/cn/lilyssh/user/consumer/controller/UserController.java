package cn.lilyssh.user.consumer.controller;

import cn.lilyssh.common.result.Result;
import cn.lilyssh.user.api.model.request.UserQueryReq;
import cn.lilyssh.user.api.model.response.UserQueryResp;
import cn.lilyssh.user.consumer.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    /**
     * 获取所有用户
     */
    @GetMapping
    public Result<List<UserQueryResp>> userList(UserQueryReq userQueryReq){
        return userService.userList(userQueryReq);
    }


    /**
     * 造数据
     */
    @GetMapping("/{userNum}/{orderNum}")
    public Result initData(@PathVariable Integer userNum,@PathVariable Integer orderNum){
        return userService.initData(userNum,orderNum);
    }
}
