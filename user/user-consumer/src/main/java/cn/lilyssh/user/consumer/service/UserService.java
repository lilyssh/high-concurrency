package cn.lilyssh.user.consumer.service;

import cn.lilyssh.common.result.Result;
import cn.lilyssh.user.api.model.request.UserQueryReq;
import cn.lilyssh.user.api.model.response.UserQueryResp;
import cn.lilyssh.user.api.service.UserServiceApi;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserService{

    @Reference
    private UserServiceApi userServiceApi;

    public Result<List<UserQueryResp>> userList(UserQueryReq userQueryReq){
        List<UserQueryResp> list = userServiceApi.list(userQueryReq);
        return Result.result(list);
    }

    public Result initData(Integer userNum,Integer orderNum){
        Thread thread = new Thread(() -> {
            userServiceApi.initData(userNum, orderNum);
        });
        thread.start();
        return Result.ok();
    }

}
