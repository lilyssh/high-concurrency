package cn.lilyssh.user.provider.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("user")
public class UserEntity {
    private int id;
    private String uuid;
    private String userName;
    private String password;
    private String realName;
    private int sex;
    private int age;
    private int phone;
    private String email;
    private int status;
    private String lastLoginIp;
    private Date lastLoginTime;
    private int idType;
    private String idNumber;
    private String address;

}
