package cn.lilyssh.user.provider.dao.mapper;

import cn.lilyssh.user.provider.dao.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

}