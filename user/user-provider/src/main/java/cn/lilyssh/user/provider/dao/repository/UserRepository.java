package cn.lilyssh.user.provider.dao.repository;

import cn.lilyssh.user.provider.dao.entity.UserEntity;
import cn.lilyssh.user.provider.dao.mapper.UserMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class UserRepository  extends ServiceImpl<UserMapper, UserEntity> {

}
