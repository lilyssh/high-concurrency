package cn.lilyssh.user.provider.service;

import cn.lilyssh.common.example.ChineseUtil;
import cn.lilyssh.common.example.CityUtil;
import cn.lilyssh.common.example.StringRandom;
import cn.lilyssh.order.api.model.request.OrderInsertReq;
import cn.lilyssh.order.api.service.OrderServiceApi;
import cn.lilyssh.user.api.model.request.UserInsertReq;
import cn.lilyssh.user.api.model.request.UserQueryReq;
import cn.lilyssh.user.api.model.response.UserQueryResp;
import cn.lilyssh.user.api.service.UserServiceApi;
import cn.lilyssh.user.provider.dao.entity.UserEntity;
import cn.lilyssh.user.provider.dao.repository.UserRepository;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;

@Slf4j
@Service
@Component
public class UserService  implements UserServiceApi {

    @Autowired
    private UserRepository userRepository;

    @Reference
    private OrderServiceApi orderServiceApi;

    @Override
    public List<UserQueryResp> list(UserQueryReq userQueryReq){
        QueryWrapper<UserEntity> qw = new QueryWrapper<>();
        if(null!=userQueryReq){
            if(!StringUtils.isEmpty(userQueryReq.getId())){
                qw.eq("id",userQueryReq.getId());
            }else{
                qw.le("id",50);
            }
        }
        List<UserEntity> userEntityList = userRepository.list(qw);
        List<UserQueryResp> resultList=new ArrayList<>();
        userEntityList.forEach(userEntity->{
            UserQueryResp  userQueryResp=new UserQueryResp();
            BeanUtils.copyProperties(userEntity,userQueryResp);
            resultList.add(userQueryResp);
        });
        return resultList;
    }

    @Override
    public boolean exist(Integer userId) {
        QueryWrapper<UserEntity> qw = new QueryWrapper<>();
        qw.eq("id",userId);
        UserEntity userEntity = userRepository.getOne(qw);
        if(null!=userEntity && userEntity.getId()>0){
            return true;
        }
        return false;
    }

    @Override
    public UserQueryResp save(UserInsertReq userInsertReq) {
        UserEntity userEntity=new UserEntity();
        BeanUtils.copyProperties(userInsertReq,userEntity);
        boolean result = userRepository.save(userEntity);
        if(result){
            UserQueryResp userQueryResp=new UserQueryResp();
            BeanUtils.copyProperties(userEntity,userQueryResp);
            return userQueryResp;
        }else{
            log.error("新增用户失败");
            return null;
        }
    }

    @Override
    public void initData(Integer userNum,Integer orderNum) {
        Random ra = new Random();
        Date now = new Date();
        for(int i=0;i<userNum;i++){
            //1、新增用户
            UserInsertReq  userInsertReq = new UserInsertReq();
            userInsertReq.setAddress(CityUtil.random());
            userInsertReq.setAge(ra.nextInt(70)+10);
            userInsertReq.setEmail(ra.nextInt(1000)*ra.nextInt(1000)+"@qq.com");
            userInsertReq.setIdNumber(ra.nextInt(1000)*ra.nextInt(1000)+"");
            userInsertReq.setIdType(1);
            userInsertReq.setLastLoginIp("127.0.0.1");
            userInsertReq.setLastLoginTime(now);
            userInsertReq.setUuid(UUID.randomUUID().toString());
            userInsertReq.setPassword(ra.nextInt(1000)+"");
            userInsertReq.setPhone(ra.nextInt(1000)*ra.nextInt(1000));
            userInsertReq.setRealName(ChineseUtil.getChinese(3));
            userInsertReq.setSex(ra.nextInt(2));
            userInsertReq.setStatus(1);
            userInsertReq.setUserName(StringRandom.getStringRandom(6));

            UserQueryResp userQueryResp = this.save(userInsertReq);

            //2、下单
            List<OrderInsertReq> orderInsertReqList = new ArrayList<>();
            for (int j = 0; j < orderNum; j++) {
                OrderInsertReq orderInsertReq = new OrderInsertReq();
                orderInsertReq.setCreateTime(now);
                orderInsertReq.setCloseTime(now);
                orderInsertReq.setCosignTime(now);
                orderInsertReq.setEndTime(now);
                orderInsertReq.setPayment(new BigDecimal(ra.nextInt(10000)));
                orderInsertReq.setPayTime(now);
                orderInsertReq.setPayType(ra.nextInt(4));
                orderInsertReq.setPostFee(new BigDecimal(ra.nextInt(20)));
                orderInsertReq.setShippingCode(StringRandom.getStringRandom(4));
                orderInsertReq.setShippingName(ChineseUtil.getChinese(2));
                orderInsertReq.setStatus(ra.nextInt(8));
                orderInsertReq.setUpdateTime(now);
                orderInsertReq.setUserId(userQueryResp.getId());
                orderInsertReq.setUserUuid(userQueryResp.getUuid());
                orderInsertReqList.add(orderInsertReq);
            }
            orderServiceApi.saveBatch(orderInsertReqList);
        }
    }

}
